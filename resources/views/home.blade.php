@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <form>
            <div class="col-md-10 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">Filtros</div>

                    <div class="panel-body">
                       <div class="form-group">
                            <label for="">Nombre</label>
                            <input type="text" class="form-control" placeholder="nombre del producto" name="nombre" value="{{old('nombre')}}">
                       </div>
                       <div class="form-group">
                            <input type="submit" class="form-control btn btn-primary" value="buscar">
                            <button @click="limpiar()" class="form-control btn btn-warning">Limpiar</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    
    <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Productos</div>

                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>Nombre</th>
                                <th>Codigo</th>
                                <th>Precio</th>
                                <th>Stock</th>
                                <th>Foto</th>
                                <th>Acción</th>

                            </thead>
                            <tbody>
                                @foreach ($productos as $producto)
                                <tr>
                                    <td>
                                        {{ $producto->nombre}}
                                    </td>
                                    <td>
                                        {{ $producto->codigo}}
                                    </td>
                                    <td>
                                        {{ $producto->precio}}
                                    </td>
                                    <td>
                                        {{ $producto->stock}}
                                    </td>
                                    <td>
                                      {{ $producto->foto}}
                                    </td>
                                    <td>
                                        <button class="btn btn-primary" @click="agregar({{$producto->id}})">Agregar al carrito</button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(($venta))
    <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Carrito 
                </div>
                
                <div class="panel-body">
                    <label for="">Venta</label>
                    <label for="">
                        @if($venta->estado == 1)
                        Pendiente
                        @elseif ($venta->estado == 2) 
                        Aprobado
                        @elseif ($venta->estado == 3) 
                        Rechazado
                        @endif
                    </label>
                    <div class="form-group">

                        <button @click="aprobar({{$venta->id}})" class="btn btn-primary form-control" > Aprobar</button>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>Nombre del producto</th>
                                <th>Cantidad</th>
                                <th>Precio</th>

                            </thead>
                            <tbody>
                                @foreach ($venta->detalles as $detalle)
                                <tr>
                                    <td>
                                        {{ isset($detalle->producto->nombre)? $detalle->producto->nombre: null}}
                                    </td>
                                    <td>
                                        {{ isset($detalle->cantidad)? $detalle->cantidad: 0}}
                                    </td>
                                    <td>
                                        {{ isset($detalle->precio)? $detalle->precio: 0}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection
@push('scripts')
<script>
    
    let token = document.head.querySelector('meta[name="csrf-token"]');

    if (token) {
        window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
    } else {
        console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
    }
    var vueApp = new Vue({
        'el': '#app',
        methods:{ 
            agregar: function(producto) {
                let confirmar = confirm('Desea continuar con la operación');
                if (confirmar) {
                    let request = new FormData();
                    request.append('producto_id', producto);
                    axios.post("{{route('venta')}}", request).
                    then(response => {
                        location.reload();
                    }).
                    catch(error => {
                        console.log(error);
                    });

                }
            }, 
            aprobar: function (venta) {
                  let confirmar = confirm('Desea continuar con la operación');
                if (confirmar) {
                    let request = new FormData();
                    request.append('venta_id', venta);
                    axios.post("{{route('venta.aprobar')}}", request).
                    then(response => {
                        location.reload();
                    }).
                    catch(error => {
                        console.log(error);
                    });

                }
            }
        }
    });
</script>
@endpush