<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Despacho extends Model
{
    protected $fillable = ['venta_id' ,'fecha'];
    
    public function ventas () {
        return $this->hasMany('App\Venta', 'venta_id', 'id');
    }

}
