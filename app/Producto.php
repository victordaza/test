<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $fillable = ['nombre' ,'descripcion' ,'codigo','stock','foto','precio' ];

    public function scopeBuscar($query, $request) {
        if($request->nombre) {
            $query->where('nombre', $request->nombre);
        }
        return $query;
    }
}
