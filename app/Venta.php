<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    protected $fillable = ['user_id',  'fecha', 'estado'];

    public function cliente(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    public function detalles(){
        return $this->hasMany('App\VentaDetalle', 'venta_id', 'id');
    }

}
