<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentaDetalle extends Model
{
    protected $table = 'venta_detalles';
    protected $fillable = ['producto_id','venta_id',  'cantidad', 'precio', 'total'];

    public function venta(){
        return $this->belongsTo('App\Venta', 'venta_id', 'id');
    }

    public function producto(){
        return $this->belongsTo('App\Producto', 'producto_id', 'id');
    }

}
