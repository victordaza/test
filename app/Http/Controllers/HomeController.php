<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Storage;
use App\Venta;
use \Exception;
use App\Producto;
use Carbon\Carbon;
use App\VentaDetalle;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $productos =  Producto::buscar($request)->get();
        $venta = Venta::
        with('detalles')
        ->where('user_id', Auth::user()->id)
        ->where('estado', 1)->first();
        return view('home')
        ->with('productos', $productos)
        ->with('venta', $venta)
        ;
    }
    public function producto (Request $request) {
        return view('producto');
    }

    public function producto_store(Request $request) {
        try {
            DB::beginTransaction();
            $producto = new Producto();
            $producto->fill($request->all());
            $file = $request->file('foto');
            $nombre = $file->getClientOriginalName();
            $producto->foto = $nombre;
            $producto->save();
            \Storage::disk('local')->put($nombre,  \File::get($file));
            DB::commit();
        } catch(\Exception $e) {
            DB::rollback();
            return redirect()->back();
        }
        return redirect()->route('home');
    }
    public function venta(Request $request) {
          try {
            DB::beginTransaction();
             $venta =  Venta::where('user_id', Auth::user()->id)->where('estado',1)->first();
            if(is_null( $venta)) {
                $venta = new Venta();
                $venta->fecha = Carbon::now();
                $venta->estado =1;
                $venta->user_id = Auth::user()->id;
                $venta->save();
            }
            $producto = Producto::find($request->producto_id);
            $ventaDetalle = VentaDetalle::where('producto_id', $producto->id )
            ->where('venta_id',  $venta->id)->first();
            if(is_null($ventaDetalle)) {
                $ventaDetalle = new VentaDetalle();
                $ventaDetalle->venta_id = $venta->id;
                $ventaDetalle->producto_id = $producto->id;
                $ventaDetalle->cantidad = 1;
                $ventaDetalle->precio = $producto->precio;
            } else {
                $ventaDetalle->cantidad = $ventaDetalle->cantidad + 1;
            }
            $ventaDetalle->total = $ventaDetalle->cantidad * $producto->precio;
            $ventaDetalle->save();
            $producto->stock = $producto->stock - 1 ;
            $producto->save();
            DB::commit();
        } catch(\Exception $e) {
            DB::rollback();
            return redirect()->back();
        }
        return redirect()->route('home');
    }
    public function aprobar(Request $request) {
         try {
            DB::beginTransaction();
            $venta =  Venta::where('id', $request->venta_id)->first();
            $venta->estado = 2;
            $venta->save();
            DB::commit();
        } catch(\Exception $e) {
            DB::rollback();
            return redirect()->back();
        }
        return redirect()->route('home');
    }
}
