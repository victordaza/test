<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::group(['middleware' => 'auth'], function() {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/producto', 'HomeController@producto')->name('producto');
    Route::post('/producto', 'HomeController@producto_store')->name('producto_store');
    Route::post('/venta', 'HomeController@venta')->name('venta');
    Route::post('/venta/aprobar', 'HomeController@aprobar')->name('venta.aprobar');

});
 Route::get('foto/{foto?}', 
 function($foto){ 
    $path = storage_path() .($foto); 

        if(!File::exists($path)) abort(404); 

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
 }); 


/* Route::get('foto/{foto}','HomeController@foto' );
 */